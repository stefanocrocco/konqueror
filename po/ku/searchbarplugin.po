# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: searchbarplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-09 00:37+0000\n"
"PO-Revision-Date: 2009-01-19 05:01+0200\n"
"Last-Translator: Omer Ensari <oensari@gmail.com>\n"
"Language-Team: Kurdish <kde-i18n-doc@kde.org>\n"
"Language: ku\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Language: Kurdish\n"
"X-Poedit-SourceCharset: utf8\n"

#: searchbar.cpp:68
#, kde-format
msgid ""
"Search Bar<p>Enter a search term. Click on the icon to change search mode or "
"provider.</p>"
msgstr ""
"Darika Lêgerînê<p>Peyva lêgerînê binivîse. Ji bo guherandina lêgerînê an jî "
"peydekerê li ser îkonê bitikîne.</p>"

#: searchbar.cpp:73
#, kde-format
msgid "Search Bar"
msgstr "Darika Lêgerînê"

#: searchbar.cpp:78
#, kde-format
msgid "Focus Searchbar"
msgstr "Nîvenda Darika Lêgerînê"

#: searchbar.cpp:279
#, fuzzy, kde-format
#| msgid "Find in This Page"
msgid "Find in Page..."
msgstr "Di Vê Rûpelê de Bibîne"

#: searchbar.cpp:299
#, kde-format
msgid "Find in This Page"
msgstr "Di Vê Rûpelê de Bibîne"

#: searchbar.cpp:311
#, kde-format
msgid "Select Search Engines..."
msgstr "Motorên Lêgerînê Hilbijêre..."

#: searchbar.cpp:330
#, kde-format
msgid "Add %1..."
msgstr ""

#. i18n: ectx: ToolBar (locationToolBar)
#: searchbarplugin.rc:3
#, kde-format
msgid "Search Toolbar"
msgstr "Darika Amûra Lêgerînê"

#: WebShortcutWidget.cpp:33
#, kde-format
msgid "Set Uri Shortcuts"
msgstr ""

#: WebShortcutWidget.cpp:47
#, kde-format
msgid "Name:"
msgstr ""

#: WebShortcutWidget.cpp:51
#, kde-format
msgid "Shortcuts:"
msgstr ""
