# translation of babelfish.po to Latvian
# translation of babelfish.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008.
# Peteris Krisjanis <pecisk@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: babelfish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-20 01:59+0000\n"
"PO-Revision-Date: 2011-07-13 23:11+0300\n"
"Last-Translator: Peteris Krisjanis <pecisk@gmail.com>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Rūdolfs Mazurs,Māris Nartišs,Pēteris Krišjānis"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "rudolfs.mazurs@gmail.com,maris.kde@gmail.com,pecisk@gmail.com"

#. i18n: ectx: Menu (tools)
#: babelfishplugin.rc:4
#, kde-format
msgid "&Tools"
msgstr "&Rīki"

#. i18n: ectx: ToolBar (extraToolBar)
#: babelfishplugin.rc:8
#, kde-format
msgid "Extra Toolbar"
msgstr "Papildus rīkjosla"

#: plugin_babelfish.cpp:38
#, kde-format
msgid "Translate Web Page"
msgstr "Tulkot tīmekļa lapu"

#: plugin_babelfish.cpp:46
#, kde-format
msgid "Translate Web &Page"
msgstr "Tulkot tīmekļa la&pu "

#: plugin_babelfish.cpp:83
#, kde-format
msgid "&English To"
msgstr "No &angļu uz"

#: plugin_babelfish.cpp:86
#, kde-format
msgid "&French To"
msgstr "No &franču uz"

#: plugin_babelfish.cpp:89
#, kde-format
msgid "&German To"
msgstr "No &vācu uz"

#: plugin_babelfish.cpp:92
#, kde-format
msgid "&Greek To"
msgstr "No &franču uz"

#: plugin_babelfish.cpp:95
#, kde-format
msgid "&Spanish To"
msgstr "No &spāņu uz"

#: plugin_babelfish.cpp:98
#, kde-format
msgid "&Portuguese To"
msgstr "No &portugāļu uz"

#: plugin_babelfish.cpp:101
#, kde-format
msgid "&Italian To"
msgstr "No &itāļu"

#: plugin_babelfish.cpp:104
#, kde-format
msgid "&Dutch To"
msgstr "No &holandiešu uz"

#: plugin_babelfish.cpp:107
#, kde-format
msgid "&Russian To"
msgstr "No k&rievu uz "

#: plugin_babelfish.cpp:126
msgid "&Chinese (Simplified)"
msgstr "Ķīniešu (vien&kāršotās)"

#: plugin_babelfish.cpp:127
msgid "Chinese (&Traditional)"
msgstr "Ķīniešu (&tradicionālas)"

#: plugin_babelfish.cpp:128 plugin_babelfish.cpp:141
msgid "&Dutch"
msgstr "&Holandiešu"

#: plugin_babelfish.cpp:129 plugin_babelfish.cpp:150 plugin_babelfish.cpp:152
#: plugin_babelfish.cpp:154 plugin_babelfish.cpp:156 plugin_babelfish.cpp:158
#: plugin_babelfish.cpp:160 plugin_babelfish.cpp:162
msgid "&French"
msgstr "&Franču"

#: plugin_babelfish.cpp:130 plugin_babelfish.cpp:143
msgid "&German"
msgstr "&Vācu"

#: plugin_babelfish.cpp:131 plugin_babelfish.cpp:144
msgid "&Greek"
msgstr "&Grieķu"

#: plugin_babelfish.cpp:132 plugin_babelfish.cpp:145
msgid "&Italian"
msgstr "&Itāļu"

#: plugin_babelfish.cpp:133
msgid "&Japanese"
msgstr "&Japāņu"

#: plugin_babelfish.cpp:134
msgid "&Korean"
msgstr "&Korejiešu"

#: plugin_babelfish.cpp:135
msgid "&Norwegian"
msgstr "&Norvēģu"

#: plugin_babelfish.cpp:136 plugin_babelfish.cpp:146
msgid "&Portuguese"
msgstr "&Portugāļu"

#: plugin_babelfish.cpp:137 plugin_babelfish.cpp:148
msgid "&Russian"
msgstr "K&rievu "

#: plugin_babelfish.cpp:138 plugin_babelfish.cpp:147
msgid "&Spanish"
msgstr "&Spāņu"

#: plugin_babelfish.cpp:139
msgid "T&hai"
msgstr "Tai&zemiešu"

#: plugin_babelfish.cpp:140
msgid "&Arabic"
msgstr "&Arābu"

#: plugin_babelfish.cpp:142 plugin_babelfish.cpp:149 plugin_babelfish.cpp:151
#: plugin_babelfish.cpp:153 plugin_babelfish.cpp:155 plugin_babelfish.cpp:157
#: plugin_babelfish.cpp:159 plugin_babelfish.cpp:161
msgid "&English"
msgstr "&Angļu"

#: plugin_babelfish.cpp:183
#, kde-format
msgid "&Chinese (Simplified) to English"
msgstr "No ķīniešu (&vienāršotās) uz angļu"

#: plugin_babelfish.cpp:184
#, kde-format
msgid "Chinese (&Traditional) to English"
msgstr "No ķīniešu (&tradicionālas) uz angļu"

#: plugin_babelfish.cpp:193
#, kde-format
msgid "&Japanese to English"
msgstr "No &japāņu uz angļu"

#: plugin_babelfish.cpp:194
#, kde-format
msgid "&Korean to English"
msgstr "No &korejiešu uz angļu"

#: plugin_babelfish.cpp:262
#, kde-format
msgctxt "@title:window"
msgid "Malformed URL"
msgstr "Slikti formatēts URL"

#: plugin_babelfish.cpp:263
#, kde-format
msgid "The URL you entered is not valid, please correct it and try again."
msgstr ""
"Jūsu ievadītais URL nav derīgs, lūdzu izlabojiet to un pamēģiniet atkal."

#: plugin_babelfish.cpp:273
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You are viewing this page over a secure connection.<nl/><nl/>The URL of the "
"page will sent to the online translation service,<nl/>which may fetch the "
"insecure version of the page."
msgstr ""

#: plugin_babelfish.cpp:277
#, kde-format
msgctxt "@title:window"
msgid "Security Warning"
msgstr ""

#~ msgid "&Russian to English"
#~ msgstr "No K&rievu uz Angļu"

#~ msgid "Cannot Translate Source"
#~ msgstr "Neizdevās pārtulkot avotu"

#~ msgid "Only web pages can be translated using this plugin."
#~ msgstr "Ar šo spraudni var tulkot tikai tīmekļa lapas."

#~ msgid "Only full webpages can be translated for this language pair."
#~ msgstr "Šajā valodu pārī var pārtulkot tikai pilnu tīmekļa lapu."

#~ msgid "Translation Error"
#~ msgstr "Tulkošanas kļūda"
