# translation of imgalleryplugin.po to Khmer
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2006, 2007, 2008.
# Auk Piseth <piseth_dv@khmeros.info>, 2007.
# Chan Sambathratanak <ratanak@khmeros.info>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: imgalleryplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-20 01:59+0000\n"
"PO-Revision-Date: 2011-12-21 08:41+0700\n"
"Last-Translator: Chan Sambathratanak <ratanak@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: WordForge 0.8 RC1\n"
"X-Language: km-KH\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ខឹម សុខែម, ម៉ន ម៉េត, សេង សុត្ថា,ចាន់ សម្បត្តិរតនៈ,​សុខ សោភា"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"khoemsokhem@khmeros.info,​​mornmet@khmeros.info,sutha@khmeros.info,"
"ratanak@khmeros.info,sophea@khmeros.info"

#. i18n: ectx: Menu (tools)
#: babelfishplugin.rc:4
#, kde-format
msgid "&Tools"
msgstr "ឧបករណ៍"

#. i18n: ectx: ToolBar (extraToolBar)
#: babelfishplugin.rc:8
#, kde-format
msgid "Extra Toolbar"
msgstr "របារ​ឧបករណ៍​បន្ថែម"

#: plugin_babelfish.cpp:38
#, kde-format
msgid "Translate Web Page"
msgstr "បកប្រែ​ទំព័រ​បណ្ដាញ​"

#: plugin_babelfish.cpp:46
#, kde-format
msgid "Translate Web &Page"
msgstr "បកប្រែ​ទំព័រ​បណ្ដាញ"

#: plugin_babelfish.cpp:83
#, kde-format
msgid "&English To"
msgstr "អង់គ្លេស​ទៅ​ជា​​"

#: plugin_babelfish.cpp:86
#, kde-format
msgid "&French To"
msgstr "បារាំង​ទៅ​ជា​"

#: plugin_babelfish.cpp:89
#, kde-format
msgid "&German To"
msgstr "អាល្លឺម៉ង់​ទៅ​ជា​"

#: plugin_babelfish.cpp:92
#, kde-format
msgid "&Greek To"
msgstr "ក្រិក​ទៅ​ជា"

#: plugin_babelfish.cpp:95
#, kde-format
msgid "&Spanish To"
msgstr "អេស្ប៉ាញ​ទៅជា"

#: plugin_babelfish.cpp:98
#, kde-format
msgid "&Portuguese To"
msgstr "ព័រទុយហ្គាល់​ទៅ​​ជា​"

#: plugin_babelfish.cpp:101
#, kde-format
msgid "&Italian To"
msgstr "អ៊ីតាលី​ទៅ​ជា​"

#: plugin_babelfish.cpp:104
#, kde-format
msgid "&Dutch To"
msgstr "ហុល្លង់​ទៅ​ជា​"

#: plugin_babelfish.cpp:107
#, kde-format
msgid "&Russian To"
msgstr "រុស្ស៊ី​ទៅ​ជា​"

#: plugin_babelfish.cpp:126
msgid "&Chinese (Simplified)"
msgstr "ចិន (សម័យ​)"

#: plugin_babelfish.cpp:127
msgid "Chinese (&Traditional)"
msgstr "ចិន​ (បុរាណ)"

#: plugin_babelfish.cpp:128 plugin_babelfish.cpp:141
msgid "&Dutch"
msgstr "ហុល្លង់​"

#: plugin_babelfish.cpp:129 plugin_babelfish.cpp:150 plugin_babelfish.cpp:152
#: plugin_babelfish.cpp:154 plugin_babelfish.cpp:156 plugin_babelfish.cpp:158
#: plugin_babelfish.cpp:160 plugin_babelfish.cpp:162
msgid "&French"
msgstr "បារាំង​"

#: plugin_babelfish.cpp:130 plugin_babelfish.cpp:143
msgid "&German"
msgstr "អាល្លឺម៉ង់"

#: plugin_babelfish.cpp:131 plugin_babelfish.cpp:144
msgid "&Greek"
msgstr "ក្រិក​"

#: plugin_babelfish.cpp:132 plugin_babelfish.cpp:145
msgid "&Italian"
msgstr "អ៊ីតាលី​"

#: plugin_babelfish.cpp:133
msgid "&Japanese"
msgstr "ជប៉ុន"

#: plugin_babelfish.cpp:134
msgid "&Korean"
msgstr "កូរ៉េ"

#: plugin_babelfish.cpp:135
msgid "&Norwegian"
msgstr "ន័រវែស"

#: plugin_babelfish.cpp:136 plugin_babelfish.cpp:146
msgid "&Portuguese"
msgstr "ព័រទុយហ្គាល់"

#: plugin_babelfish.cpp:137 plugin_babelfish.cpp:148
msgid "&Russian"
msgstr "រុស្ស៊ី​"

#: plugin_babelfish.cpp:138 plugin_babelfish.cpp:147
msgid "&Spanish"
msgstr "អេស្ប៉ាញ"

#: plugin_babelfish.cpp:139
msgid "T&hai"
msgstr "ថៃ"

#: plugin_babelfish.cpp:140
msgid "&Arabic"
msgstr "អារ៉ាប់"

#: plugin_babelfish.cpp:142 plugin_babelfish.cpp:149 plugin_babelfish.cpp:151
#: plugin_babelfish.cpp:153 plugin_babelfish.cpp:155 plugin_babelfish.cpp:157
#: plugin_babelfish.cpp:159 plugin_babelfish.cpp:161
msgid "&English"
msgstr "អង់គ្លេស"

#: plugin_babelfish.cpp:183
#, kde-format
msgid "&Chinese (Simplified) to English"
msgstr "ចិន​ (សម័យ​​) ទៅ​ជា​អង់គ្លេស​"

#: plugin_babelfish.cpp:184
#, kde-format
msgid "Chinese (&Traditional) to English"
msgstr "ចិន (​បុរាណ) ​ទៅ​ជា​អង់គ្លេស​"

#: plugin_babelfish.cpp:193
#, kde-format
msgid "&Japanese to English"
msgstr "ជប៉ុន​ទៅ​ជា​អង់គ្លេស​"

#: plugin_babelfish.cpp:194
#, kde-format
msgid "&Korean to English"
msgstr "កូរ៉េ​ទៅជា​​អង់គ្លេស​"

#: plugin_babelfish.cpp:262
#, kde-format
msgctxt "@title:window"
msgid "Malformed URL"
msgstr "URL មិន​ត្រឹមត្រូវ​"

#: plugin_babelfish.cpp:263
#, kde-format
msgid "The URL you entered is not valid, please correct it and try again."
msgstr "URL ដែល​អ្នក​បាន​បញ្ចូល​មិន​ត្រឹមត្រូវ​ទេ​ ​សូម​កែ​តម្រូវ​វា​ ហើយ​​សាកល្បង​ម្ដងទៀត ។​"

#: plugin_babelfish.cpp:273
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You are viewing this page over a secure connection.<nl/><nl/>The URL of the "
"page will sent to the online translation service,<nl/>which may fetch the "
"insecure version of the page."
msgstr ""

#: plugin_babelfish.cpp:277
#, kde-format
msgctxt "@title:window"
msgid "Security Warning"
msgstr ""

#~ msgid "Configure"
#~ msgstr "កំណត់​រចនាសម្ព័ន្ធ"

#~ msgid "Create Image Gallery"
#~ msgstr "​បង្កើត​វិចិត្រសាល​រូបភាព"

#~ msgid "Create"
#~ msgstr "បង្កើត​"

#~ msgid "Image Gallery for %1"
#~ msgstr "វិចិត្រសាល​រូបភាព​សម្រាប់ %1"

#~ msgid "Look"
#~ msgstr "រូបរាង"

#~ msgid "Page Look"
#~ msgstr "រូបរាង​ទំព័រ"

#~ msgid "&Page title:"
#~ msgstr "ចំណងជើង​ទំព័រ ៖"

#~ msgid "I&mages per row:"
#~ msgstr "រូបភាព​ក្នុង​មួយ​ជួរ​ដេក ៖"

#~ msgid "Show image file &name"
#~ msgstr "បង្ហាញ​ឈ្មោះ​ឯកសារ​រូបភាព"

#~ msgid "Show image file &size"
#~ msgstr "បង្ហាញ​ទំហំ​ឯកសារ​រូបភាព"

#~ msgid "Show image &dimensions"
#~ msgstr "បង្ហាញ​វិមាត្រ​រូបភាព"

#~ msgid "Fon&t name:"
#~ msgstr "ឈ្មោះ​ពុម្ពអក្សរ​ ៖"

#~ msgid "Font si&ze:"
#~ msgstr "​ទំហំ​ពុម្ពអក្សរ ៖"

#~ msgid "&Foreground color:"
#~ msgstr "​ពណ៌​ផ្ទៃ​ខាង​មុខ ៖"

#~ msgid "&Background color:"
#~ msgstr "​ពណ៌​ផ្ទៃ​ខាង​ក្រោយ ៖"

#~ msgid "Folders"
#~ msgstr "​ថត"

#~ msgid "&Save to HTML file:"
#~ msgstr "រក្សាទុក​ទៅ​​​ឯកសារ​ HTML ៖"

#~ msgid "<p>The name of the HTML file this gallery will be saved to.</p>"
#~ msgstr "<p>ឈ្មោះ​របស់​ឯកសារ HTML វិចិត្រសាល​នេះ​នឹង​ត្រូវ​បាន​រក្សាទុក​ទៅ​កាន់ ។</p>"

#~ msgid "&Recurse subfolders"
#~ msgstr "ការ​ហៅ​ថតរង​ដោយ​ខ្លួន​ឯង"

#~ msgid ""
#~ "<p>Whether subfolders should be included for the image gallery creation "
#~ "or not.</p>"
#~ msgstr "<p>ថាតើ​ថតរង​គួរ​ត្រូវ​បាន​រួម​បញ្ចូល​សម្រាប់​ការ​បង្កើត​វិចិត្រសាល​រូបភាព​ ឬ​ក៏​អត់ ។</p>"

#~ msgid "Rec&ursion depth:"
#~ msgstr "ជម្រៅ​ការ​ហៅ​ខ្លួន​ឯង ៖"

#~ msgid "Endless"
#~ msgstr "គ្មាន​ទី​បញ្ចប់"

#~ msgid ""
#~ "<p>You can limit the number of folders the image gallery creator will "
#~ "traverse to by setting an upper bound for the recursion depth.</p>"
#~ msgstr ""
#~ "<p>អ្នក​អាច​កំណត់​ចំនួន​ថតនូវ​​កម្មវិធី​បង្កើត​វិចិត្រសាល​រូបភាព​នឹង​ឆ្លង​កាត់​ដោយ​​កំណត់​​ព្រំដែន​ខាង​លើ​សម្រាប់​ជម្រៅ​"
#~ "ការហៅ​ខ្លួនឯង ។</p>"

#~ msgid "Copy or&iginal files"
#~ msgstr "ចម្លង​ឯកសារ​ដើម"

#~ msgid ""
#~ "<p>This makes a copy of all images and the gallery will refer to these "
#~ "copies instead of the original images.</p>"
#~ msgstr ""
#~ "<p>វា​ធ្វើ​ការ​ចម្លង​រូបភាព​ទាំង​អស់​ ហើយ​វិចិត្រសាល​នឹង​​យោង​​លើ​ច្បាប់​ចម្លង​ជំនួស​​ឲ្យ​រូបភាព​ដើម ។</p>"

#~ msgid "Use &comment file"
#~ msgstr "ប្រើ​ឯកសារ​សេចក្តី​អធិប្បាយ ៖"

#~ msgid ""
#~ "<p>If you enable this option you can specify a comment file which will be "
#~ "used for generating subtitles for the images.</p><p>For details about the "
#~ "file format please see the \"What's This?\" help below.</p>"
#~ msgstr ""
#~ "<p>ប្រសិន​បើអ្នក​បើក​ជម្រើស​នេះ អ្នក​អាច​បញ្ជាក់​ឯកសារ​សេចក្ដីអធិប្បាយ​ដែល​នឹង​ត្រូវ​បានប្រើ​សម្រាប់​"
#~ "បង្កើត​ចំណង​ជើង​រងសម្រាប់​រូបភាព ។</p><p>សម្រាប់​សេចក្ដី​លម្អិត​​អំពី​ទ្រង់ទ្រាយ​ឯកសារ សូម​មើលជំនួយ "
#~ "\"នេះ​ជា​អ្វី?\" ខាង​ក្រោម ។</p>"

#~ msgid "Comments &file:"
#~ msgstr "ឯកសារ​សេចក្តី​អធិប្បាយ ៖"

#~ msgid ""
#~ "<p>You can specify the name of the comment file here. The comment file "
#~ "contains the subtitles for the images. The format of this file is:</"
#~ "p><p>FILENAME1:<br />Description<br /><br />FILENAME2:<br /"
#~ ">Description<br /><br />and so on</p>"
#~ msgstr ""
#~ "<p>អ្នក​អាច​បញ្ជាក់​ឈ្មោះ​របស់​ឯកសារ​អធិប្បាយ​នៅ​ទី​នេះ ។ ឯកសារ​អធិប្បាយ​មាន​ចំណង​ជើង​រង​សម្រាប់​"
#~ "រូពភាព ។ ទ្រង់ទ្រាយ​របស់​ឯកសារ​នេះ​គឺ ៖</p><p>FILENAME1:<br />សេចក្តី​ពិពណ៌នា<br /><br /"
#~ ">FILENAME2:<br />សេចក្តី​អធិប្បាយ<br /><br />និង​បន្តបន្ទាប់</p>"

#~ msgid "Thumbnails"
#~ msgstr "​រូបភាព​តូច"

#~ msgid "Image format f&or the thumbnails:"
#~ msgstr "ទ្រង់ទ្រាយ​រូបភាព​សម្រាប់​រូបភាព​តូច ៖"

#~ msgid "Thumbnail size:"
#~ msgstr "ទំហំ​រូបភាព​តូច ៖"

#~ msgid "&Set different color depth:"
#~ msgstr "កំណត់​ជម្រៅ​ពណ៌​ខុសៗគ្នា ៖"

#~ msgid "&Create Image Gallery..."
#~ msgstr "បង្កើត​វិចិត្រសាល​រូបភាព..."

#~ msgid "Could not create the plugin, please report a bug."
#~ msgstr "មិន​អាច​បង្កើត​កម្មវិធី​ជំនួយ​បានទេ សូម​រាយការណ៍​កំហុស​ ។"

#~ msgid "Creating an image gallery works only on local folders."
#~ msgstr "ការ​បង្កើត​វិចិត្រ​សាល​​រូបភាព​មួយ​​​ធ្វើការ​តែ​នៅ​លើ​ថត​មូលដ្ឋាន​ប៉ុណ្ណោះ ។"

#~ msgid "Creating thumbnails"
#~ msgstr "ការ​បង្កើត​រូបភាព​តូចៗ"

#~ msgid "Could not create folder: %1"
#~ msgstr "មិន​អាច​បង្កើត​ថត ៖ %1 បាន​ទេ"

#~ msgid "<i>Number of images</i>: %1"
#~ msgstr "<i>ចំនួន​រូបភាព</i> ៖ %1"

#~ msgid "<i>Created on</i>: %1"
#~ msgstr "<i>បាន​បង្កើត​នៅ</i> ៖ %1"

#~ msgid "<i>Subfolders</i>:"
#~ msgstr "<i>ថត​រង</i> ៖"

#~ msgid ""
#~ "Created thumbnail for: \n"
#~ "%1"
#~ msgstr ""
#~ "បាន​បង្កើត​រូបភាព​តូចៗ​សម្រាប់ ៖ \n"
#~ "%1"

#~ msgid ""
#~ "Creating thumbnail for: \n"
#~ "%1\n"
#~ " failed"
#~ msgstr ""
#~ "ការ​បង្កើត​រូបភាព​តូចៗ​​សម្រាប់ ៖ \n"
#~ "%1\n"
#~ " បាន​បរាជ័យ"

#~ msgid "KB"
#~ msgstr "គីឡូបៃ"

#~ msgid "Could not open file: %1"
#~ msgstr "មិន​អាច​បើក​ឯកសារ ៖ %1"
