msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-20 01:59+0000\n"
"PO-Revision-Date: 2024-03-28 19:17\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/konqueror/babelfish.pot\n"
"X-Crowdin-File-ID: 48602\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE China, Guo Yunhe"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-china@kde.org, i@guoyunhe.me"

#. i18n: ectx: Menu (tools)
#: babelfishplugin.rc:4
#, kde-format
msgid "&Tools"
msgstr "工具(&T)"

#. i18n: ectx: ToolBar (extraToolBar)
#: babelfishplugin.rc:8
#, kde-format
msgid "Extra Toolbar"
msgstr "额外工具栏"

#: plugin_babelfish.cpp:38
#, kde-format
msgid "Translate Web Page"
msgstr "翻译网页"

#: plugin_babelfish.cpp:46
#, kde-format
msgid "Translate Web &Page"
msgstr "翻译网页(&P)"

#: plugin_babelfish.cpp:83
#, kde-format
msgid "&English To"
msgstr "英语译为(&E)"

#: plugin_babelfish.cpp:86
#, kde-format
msgid "&French To"
msgstr "法语译为(&F)"

#: plugin_babelfish.cpp:89
#, kde-format
msgid "&German To"
msgstr "德语译为(&G)"

#: plugin_babelfish.cpp:92
#, kde-format
msgid "&Greek To"
msgstr "希腊语译为(&G)"

#: plugin_babelfish.cpp:95
#, kde-format
msgid "&Spanish To"
msgstr "西班牙语译为(&S)"

#: plugin_babelfish.cpp:98
#, kde-format
msgid "&Portuguese To"
msgstr "葡萄牙语译为(&P)"

#: plugin_babelfish.cpp:101
#, kde-format
msgid "&Italian To"
msgstr "意大利语译为(&I)"

#: plugin_babelfish.cpp:104
#, kde-format
msgid "&Dutch To"
msgstr "荷兰语译为(&D)"

#: plugin_babelfish.cpp:107
#, kde-format
msgid "&Russian To"
msgstr "俄语译为(&R)"

#: plugin_babelfish.cpp:126
msgid "&Chinese (Simplified)"
msgstr "简体中文(&C)"

#: plugin_babelfish.cpp:127
msgid "Chinese (&Traditional)"
msgstr "繁体中文(&T)"

#: plugin_babelfish.cpp:128 plugin_babelfish.cpp:141
msgid "&Dutch"
msgstr "荷兰语(&D)"

#: plugin_babelfish.cpp:129 plugin_babelfish.cpp:150 plugin_babelfish.cpp:152
#: plugin_babelfish.cpp:154 plugin_babelfish.cpp:156 plugin_babelfish.cpp:158
#: plugin_babelfish.cpp:160 plugin_babelfish.cpp:162
msgid "&French"
msgstr "法语(&F)"

#: plugin_babelfish.cpp:130 plugin_babelfish.cpp:143
msgid "&German"
msgstr "德语(&G)"

#: plugin_babelfish.cpp:131 plugin_babelfish.cpp:144
msgid "&Greek"
msgstr "希腊语(&G)"

#: plugin_babelfish.cpp:132 plugin_babelfish.cpp:145
msgid "&Italian"
msgstr "意大利语(&I)"

#: plugin_babelfish.cpp:133
msgid "&Japanese"
msgstr "日语(&J)"

#: plugin_babelfish.cpp:134
msgid "&Korean"
msgstr "朝鲜语(&K)"

#: plugin_babelfish.cpp:135
msgid "&Norwegian"
msgstr "挪威语(&N)"

#: plugin_babelfish.cpp:136 plugin_babelfish.cpp:146
msgid "&Portuguese"
msgstr "葡萄牙语(&P)"

#: plugin_babelfish.cpp:137 plugin_babelfish.cpp:148
msgid "&Russian"
msgstr "俄语(&R)"

#: plugin_babelfish.cpp:138 plugin_babelfish.cpp:147
msgid "&Spanish"
msgstr "西班牙语(&S)"

#: plugin_babelfish.cpp:139
msgid "T&hai"
msgstr "泰语(&H)"

#: plugin_babelfish.cpp:140
msgid "&Arabic"
msgstr "阿拉伯语(&A)"

#: plugin_babelfish.cpp:142 plugin_babelfish.cpp:149 plugin_babelfish.cpp:151
#: plugin_babelfish.cpp:153 plugin_babelfish.cpp:155 plugin_babelfish.cpp:157
#: plugin_babelfish.cpp:159 plugin_babelfish.cpp:161
msgid "&English"
msgstr "英语(&E)"

#: plugin_babelfish.cpp:183
#, kde-format
msgid "&Chinese (Simplified) to English"
msgstr "简体中文译为英语(&C)"

#: plugin_babelfish.cpp:184
#, kde-format
msgid "Chinese (&Traditional) to English"
msgstr "繁体中文译为英语(&T)"

#: plugin_babelfish.cpp:193
#, kde-format
msgid "&Japanese to English"
msgstr "日语译为英语(&J)"

#: plugin_babelfish.cpp:194
#, kde-format
msgid "&Korean to English"
msgstr "朝鲜语译为英语(&K)"

#: plugin_babelfish.cpp:262
#, kde-format
msgctxt "@title:window"
msgid "Malformed URL"
msgstr "格式异常的 URL"

#: plugin_babelfish.cpp:263
#, kde-format
msgid "The URL you entered is not valid, please correct it and try again."
msgstr "您输入的 URL 无效，请更正后再试。"

#: plugin_babelfish.cpp:273
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You are viewing this page over a secure connection.<nl/><nl/>The URL of the "
"page will sent to the online translation service,<nl/>which may fetch the "
"insecure version of the page."
msgstr ""
"您正在通过安全连接查看此页面。<nl/><nl/>此页面的 URL 会被发送给在线翻译服务，"
"<nl/>但该服务可能会获取此页面的不安全版本。"

#: plugin_babelfish.cpp:277
#, kde-format
msgctxt "@title:window"
msgid "Security Warning"
msgstr "安全警告"
