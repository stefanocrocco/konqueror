# translation of babelfish.po to Slovak
# Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>, 2001-2002.
# Stanislav Visnovsky <stano@ms.mff.cuni.cz>, 2002.
# Stanislav Visnovsky <visnovsky@kde.org>, 2003, 2004.
# Roman Paholik <wizzardsk@gmail.com>, 2017, 2022.
msgid ""
msgstr ""
"Project-Id-Version: babelfish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-20 01:59+0000\n"
"PO-Revision-Date: 2022-04-09 15:17+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stanislav Višňovský"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "visnovsky@nenya.ms.mff.cuni.cz"

#. i18n: ectx: Menu (tools)
#: babelfishplugin.rc:4
#, kde-format
msgid "&Tools"
msgstr "&Nástroje"

#. i18n: ectx: ToolBar (extraToolBar)
#: babelfishplugin.rc:8
#, kde-format
msgid "Extra Toolbar"
msgstr "Extra panel nástrojov"

#: plugin_babelfish.cpp:38
#, kde-format
msgid "Translate Web Page"
msgstr "Preložiť stránku WWW"

#: plugin_babelfish.cpp:46
#, kde-format
msgid "Translate Web &Page"
msgstr "&Preložiť stránku WWW"

#: plugin_babelfish.cpp:83
#, kde-format
msgid "&English To"
msgstr "&Angličtina do"

#: plugin_babelfish.cpp:86
#, kde-format
msgid "&French To"
msgstr "&Francúzština do"

#: plugin_babelfish.cpp:89
#, kde-format
msgid "&German To"
msgstr "&Nemčina do"

#: plugin_babelfish.cpp:92
#, kde-format
msgid "&Greek To"
msgstr "Gréčtina do"

#: plugin_babelfish.cpp:95
#, kde-format
msgid "&Spanish To"
msgstr "Šp&anielština do"

#: plugin_babelfish.cpp:98
#, kde-format
msgid "&Portuguese To"
msgstr "&Portugalština do"

#: plugin_babelfish.cpp:101
#, kde-format
msgid "&Italian To"
msgstr "&Taliančina do"

#: plugin_babelfish.cpp:104
#, kde-format
msgid "&Dutch To"
msgstr "&Holandština do"

#: plugin_babelfish.cpp:107
#, kde-format
msgid "&Russian To"
msgstr "Ruština do"

#: plugin_babelfish.cpp:126
msgid "&Chinese (Simplified)"
msgstr "Čí&ština (zjednodušená)"

#: plugin_babelfish.cpp:127
msgid "Chinese (&Traditional)"
msgstr "Číština (t&radičná)"

#: plugin_babelfish.cpp:128 plugin_babelfish.cpp:141
msgid "&Dutch"
msgstr "&Holandština"

#: plugin_babelfish.cpp:129 plugin_babelfish.cpp:150 plugin_babelfish.cpp:152
#: plugin_babelfish.cpp:154 plugin_babelfish.cpp:156 plugin_babelfish.cpp:158
#: plugin_babelfish.cpp:160 plugin_babelfish.cpp:162
msgid "&French"
msgstr "&Francúzština"

#: plugin_babelfish.cpp:130 plugin_babelfish.cpp:143
msgid "&German"
msgstr "&Nemčina"

#: plugin_babelfish.cpp:131 plugin_babelfish.cpp:144
msgid "&Greek"
msgstr "Gréčtina"

#: plugin_babelfish.cpp:132 plugin_babelfish.cpp:145
msgid "&Italian"
msgstr "&Taliančina"

#: plugin_babelfish.cpp:133
msgid "&Japanese"
msgstr "&Japonština"

#: plugin_babelfish.cpp:134
msgid "&Korean"
msgstr "&Kórejština"

#: plugin_babelfish.cpp:135
msgid "&Norwegian"
msgstr "&Nórština"

#: plugin_babelfish.cpp:136 plugin_babelfish.cpp:146
msgid "&Portuguese"
msgstr "&Portugalština"

#: plugin_babelfish.cpp:137 plugin_babelfish.cpp:148
msgid "&Russian"
msgstr "&Ruština"

#: plugin_babelfish.cpp:138 plugin_babelfish.cpp:147
msgid "&Spanish"
msgstr "Š&panielština"

#: plugin_babelfish.cpp:139
msgid "T&hai"
msgstr "&Thajština"

#: plugin_babelfish.cpp:140
msgid "&Arabic"
msgstr "Arabčina"

#: plugin_babelfish.cpp:142 plugin_babelfish.cpp:149 plugin_babelfish.cpp:151
#: plugin_babelfish.cpp:153 plugin_babelfish.cpp:155 plugin_babelfish.cpp:157
#: plugin_babelfish.cpp:159 plugin_babelfish.cpp:161
msgid "&English"
msgstr "&Angličtina"

#: plugin_babelfish.cpp:183
#, kde-format
msgid "&Chinese (Simplified) to English"
msgstr "Čí&nština (zjednodušená) do angličtiny"

#: plugin_babelfish.cpp:184
#, kde-format
msgid "Chinese (&Traditional) to English"
msgstr "Čínština (&tradičná) do angličtiny"

#: plugin_babelfish.cpp:193
#, kde-format
msgid "&Japanese to English"
msgstr "&Japončina do angličtiny"

#: plugin_babelfish.cpp:194
#, kde-format
msgid "&Korean to English"
msgstr "&Kórejčina do angličtiny"

#: plugin_babelfish.cpp:262
#, kde-format
msgctxt "@title:window"
msgid "Malformed URL"
msgstr "Neplatné URL"

#: plugin_babelfish.cpp:263
#, kde-format
msgid "The URL you entered is not valid, please correct it and try again."
msgstr "Zadané URL nie je platné, prosím, opravte ho a skúste to znovu."

#: plugin_babelfish.cpp:273
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You are viewing this page over a secure connection.<nl/><nl/>The URL of the "
"page will sent to the online translation service,<nl/>which may fetch the "
"insecure version of the page."
msgstr ""
"Prezeráte stránku cez zabezpečené pripojenie.<nl/><nl/> URL stránky sa pošle "
"do online prekladovej služby, ktorá môže stiahnuť nezabezpečenú verziu "
"stránky."

#: plugin_babelfish.cpp:277
#, kde-format
msgctxt "@title:window"
msgid "Security Warning"
msgstr "Bezpečnostné upozornenie"
